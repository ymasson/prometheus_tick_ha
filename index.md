---
title: Prometheus & TICK in Production
subtitle:
description:
author: Yann Masson
theme: moon
separator: <!--s-->
revealOptions:
  align: left
  width: 1280
  height: 1024
  margin: 0.04
  center: false
  transition: 'fade'
---

## Prometheus & TICK in Production

<!--s-->
## Le problème
Lorsque un incident survient en Production, l'un des seuls moyens de voir
ce qu'il se passe est le système de supervision et de métrologie.

Il est donc indispensable que cette partie de l'infrastructure soit particulèrement hautement disponible et résiliente.

<!--s-->
## Prometheus
Prometheus a une architecture distribuée.

Cela induit que chaque élément composant la stack Prometheus n'a pas de mode cluster. Pour la haute disponibilité, nous allons devoir multiplier les instances de chaque éléments.

Cela induit également la multiplication des points collectés. Nous allons donc avoir besoin de les concentrer.

Prometheus serveur n'a pas de mécanisme de rétention longue des metriques.

Pour répondre simplement à tous ces points liés à un système distribué, nous allons utiliser Thanos.

La résilience est garantie par son architecture.

<!--s-->
## Thanos
Thanos est un outil Open Source écrit en Go qui se concentre sur le stockage, le traitement et la mise à disposition à travers une API, de gros volumes de Time Series dans un stockage objet (CEPH, S3..).

Il est composé de 5 éléments:
* Sidecar, co-localisé à Prometheus, il collecte les metriques et les stocke
* Querier, expose l'API. Intéroge les sidecar et le stockage
* Store, permet les requetes sur le stockage
* Compacter, maintenance sur le stockage. (compression, purge, etc)
* Ruler, règles globales

<!--s-->
## Thanos
![thanos_architecture](images/thanos_architecture.jpg)<!-- .element width="100%" -->

<!--s-->
## Prometheus en haute disponibilité
![prometheus_ha](images/prometheus_ha.png)<!-- .element width="100%" -->

<!--s-->
## TICK
La stack TICK a une approche plus traditionnelle et centralisée.

Chaque élément qui la compose est prévu pour travailler en cluster. On devra donc ajouter des outils, comme HAproxy et Keepalived (VRRP), pour fournir la haute disponibilité.

Cependant, son architeture centralisée reporte sa résilience sur l'infrastructure sous jacente. Ce qui peu la rendre moins facile à mettre à l'échelle et à rendre robuste.

<!--s-->
## TICK en haute disponibilité
![tick_ha](images/tick_ha.png)<!-- .element width="100%" -->

<!--s-->
## Résumons
* Prometheus necessite l'ajout de Thanos et demande à revoir un peu l'implémentation
* TICK ne demande pas de changement majeur
* La résilience de Prometheus est celle du stockage S3
* La résilience de TICK est celle de sa TSDB
* Prometheus, Thanos et S3 n'ont pas de coût de licence
* InfluxDB necessite une licence pour permettre le mode cluster
* Thanos implique un coût de traitement et de stockage puisque nous avons autant de Series que de Prometheus
* la TSDB de TICK doit faire l'objet d'une extême vigilance.
